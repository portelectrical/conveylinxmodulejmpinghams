TYPE Motor

VAR
StatusInput: INT;
PositionInput: INT;
Run: BOOL;
Speed: INT;
Direction: BOOL;
Accel: INT;
Decel: INT;
PositionSaved: INT;
DistCalc: INT;
DistAccumCalc: DINT;
DistAccumCalcAbsolute: DINT;
DistLimit: DINT;
DistTarget: DINT;
END_VAR

END_TYPE
