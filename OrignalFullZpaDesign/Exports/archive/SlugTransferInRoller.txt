FUNCTION_BLOCK SlugTransferInRoller


VAR_INPUT
Initialise: BOOL;
FromUpstream: INT;
FromDownstream: INT;
FromPLC: INT;
SpeedPercent: INT;
END_VAR

VAR_OUTPUT
CONST: CONST;

ZoneExists: BOOL;
PhotoEyeExists: BOOL;
MotorExists: BOOL;

NextState: INT;
CurrentState: INT;
FaultCode: INT;

ToUpstream: INT;
ToDownstream: INT;

wdt: DINT;
wdtTarget: DINT;


DischargeSpeed: INT;
AcceptSpeed: INT;
OverrunDist: DINT;
AcceptGiveupDist: DINT;
MotorFailDist: DINT;
AccelDist: INT;



PhotoEye: PhotoEye;
IO_Module: IO_Module;
Motor: Motor;
END_VAR

VAR

SPEED_m_min: INT;

ZONE_LENGTH_mm: INT;
OVERRUN_LENGTH_mm: INT;
ACCEL_LENGTH_mm: INT;
	
MAX_SPEED: INT;
COUNTS_PER_MM_X10: INT;	
SPEED_FACTOR_X10: INT;

END_VAR

		
/* ***************************** Initialise variables ***************************** */
IF(Initialise) THEN
//Define all constants
	CONST();
	
	//Motor Control
	ZONE_LENGTH_mm := 1500;
	OVERRUN_LENGTH_mm := 500;
	ACCEL_LENGTH_mm := 100;

	//Motor type selection goes here MDR vs PGD and M8 vs JST
	MAX_SPEED := CONST.MAX_SPEED_MDR;
	COUNTS_PER_MM_X10 := CONST.COUNTS_PER_MM_X10_MDR_M8;
	SPEED_FACTOR_X10 := CONST.SPEED_FACTOR_X10_MDR_M8;
	

	SPEED_m_min := MAX_SPEED * SpeedPercent / 100;
	
	DischargeSpeed := SPEED_m_min * SPEED_FACTOR_X10/10; 
	AcceptSpeed := DischargeSpeed * 9/10;
		
	OverrunDist := OVERRUN_LENGTH_mm *COUNTS_PER_MM_X10/10;
	AcceptGiveupDist := ZONE_LENGTH_mm *COUNTS_PER_MM_X10/10 *3/2;
 	MotorFailDist := AcceptGiveupDist * 5;		
		
	AccelDist := ACCEL_LENGTH_mm *COUNTS_PER_MM_X10/10; 
	
///////// Now configure the variables using the above constants /////////
	


//IO_Module States
	IO_Module.Mode := CONST.INPUT;
	IO_Module.StateOutput := CONST.LOW;


//Motor States
	Motor.Run := CONST.STOP;
	Motor.Speed := DischargeSpeed;
	Motor.Direction := CONST.FWD;

	Motor.Accel := AccelDist;
	Motor.Decel := AccelDist;

	//Reset the motor distance calculation variables
 	Motor.PositionSaved := Motor.PositionInput;
	Motor.DistAccumCalc := 0;

//Watchdog timer
	wdt := 0; 
	wdtTarget := CONST.WDT_TIMEOUT; //initialise target to safe value

//Control States
	CurrentState := CONST.UNKNOWN;
	NextState := CONST.UNKNOWN;
	FaultCode := CONST.UNKNOWN;
	
	
//Specify whether components exist or not
	//Set to 0 to make non-existant
	ZoneExists := 1; 	
	PhotoEyeExists := 1; 
	MotorExists := 1;

END_IF;

/* ***************************** Initialisation Complete ***************************** */


/* ***************************** Outside FSM (ALWAYS EXECUTED) ***************************** */

//Update the states
CurrentState := NextState;

//Set the default content sent to upstream and downstream
ToUpstream := CurrentState;
ToDownstream := CurrentState;

//Set default Motor status
Motor.Run := CONST.STOP;

//Set default IO_Module state
IO_Module.StateOutput := CONST.LOW;

//Increase the watchdog timer
wdt := wdt + 1;

//Calculate the accumulated distance moved by the roller for overrun and timeouts (distance run-out)
Motor.DistCalc := Motor.PositionInput - Motor.PositionSaved;
IF(Motor.DistCalc > 1000) THEN
	Motor.DistCalc := Motor.DistCalc - 32768; //2^16 = 32768
ELSIF(Motor.DistCalc < -1000) THEN
	Motor.DistCalc := Motor.DistCalc + 32768;
END_IF;

Motor.DistAccumCalc := Motor.DistAccumCalc + Motor.DistCalc;
Motor.DistAccumCalcAbsolute := ABS(Motor.DistAccumCalc);
Motor.PositionSaved := Motor.PositionInput; //ready for next cycle

/* ***************************** Start of functional FSM ***************************** */


//Starting state is CONST.UNKNOWN, here it needs to figure out what is on the zone
IF(CurrentState = CONST.UNKNOWN) THEN
	//Motor.Run := CONST.STOP;

	NextState := CONST.ACCEPT_INIT;
	
	
ELSIF(CurrentState = CONST.UNKNOWN_SAFE) THEN
	//Motor.Run := CONST.STOP;

	Motor.DistAccumCalc := 0;
	wdt := 0;  //stable state so reset watchdog

	IF((FromDownstream = CONST.EMPTY)) THEN
		//once upsteam considers it safe we can try to do stuff
		NextState := CONST.ACCEPT_INIT;
	END_IF;


ELSIF(CurrentState = CONST.EMPTY) THEN
	//Motor.Run := CONST.STOP;
	
	Motor.DistAccumCalc := 0;
	wdt := 0; //Stable state so want to reset the watchdog timer
	
	IF(PhotoEye.State = CONST.BLOCKED) THEN
		NextState := CONST.UNKNOWN_SAFE;
	ELSIF((FromUpstream >= CONST.DISCHARGE) AND (FromUpstream < CONST.LOADED)) THEN
		NextState := CONST.ACCEPT_INIT;
	END_IF;


ELSIF(CurrentState = CONST.ACCEPT_INIT) THEN 
	//Motor.Run := CONST.STOP;

	Motor.Speed := AcceptSpeed;
	
	Motor.DistTarget := Motor.DistAccumCalcAbsolute + AcceptGiveupDist;
	
	NextState := CONST.ACCEPT;


ELSIF(CurrentState = CONST.ACCEPT) THEN
	Motor.Run := CONST.RUN;
	
	IF(PhotoEye.State = CONST.BLOCKED) THEN
		NextState := CONST.ACCEPT_OVERRUN_INIT;
	ELSIF(Motor.DistAccumCalcAbsolute >= Motor.DistTarget) THEN
		NextState := CONST.DISCHARGE_WAIT;
	END_IF;
	

ELSIF(CurrentState = CONST.ACCEPT_OVERRUN_INIT) THEN
	Motor.Run := CONST.RUN;
	
	Motor.Speed := AcceptSpeed / 2;
	
	Motor.DistTarget := Motor.DistAccumCalcAbsolute + OverrunDist;
	
	NextState := CONST.ACCEPT_OVERRUN;
	
	
ELSIF(CurrentState = CONST.ACCEPT_OVERRUN) THEN
	Motor.Run := CONST.RUN;
	
	IF(Motor.DistAccumCalcAbsolute >= Motor.DistTarget) THEN
		NextState := CONST.LOADED;
	END_IF;


ELSIF(CurrentState = CONST.LOADED) THEN
	//Motor.Run := CONST.STOP;

	Motor.Speed := DischargeSpeed + 50;
	
	wdt := 0; //Stable state so want to reset the watchdog timer
	
	IF(PhotoEye.State = CONST.UNBLOCKED) THEN
		NextState := CONST.UNKNOWN_SAFE;
	ELSIF(FromDownstream = CONST.EMPTY) THEN
		NextState := CONST.DISCHARGE;
	END_IF;


ELSIF(CurrentState = CONST.DISCHARGE) THEN
	//Motor.Run := CONST.STOP;
	
	wdt := 0; //Stable state so want to reset the watchdog timer
	
	IF(FromDownstream <> CONST.EMPTY) THEN
		NextState := CONST.DISCHARGE_WAIT;
	END_IF;

ELSIF(CurrentState = CONST.DISCHARGE_WAIT) THEN
	//Motor.Run := CONST.STOP;
	
	wdt := 0; //Stable state so want to reset the watchdog timer
	
	ToDownstream := CONST.EMPTY;
	
	IF(FromDownstream = CONST.EMPTY) THEN
		NextState := CONST.DISCHARGE_DROP;
	END_IF;


ELSIF(CurrentState = CONST.DISCHARGE_DROP) THEN
	//Motor.Run := CONST.STOP;
	
	wdt := 0; //Stable state so want to reset the watchdog timer
	
	ToDownstream := CONST.EMPTY;
		
	IF(PhotoEye.State = CONST.BLOCKED) THEN
		NextState := CONST.UNKNOWN_SAFE; 
	ELSE
		NextState := CONST.EMPTY;		
	END_IF;


//Catch all state in case something goes wrong
ELSE
	//Motor.Run := CONST.STOP;

	FaultCode := CurrentState;

	NextState := CONST.UNKNOWN_SAFE;
END_IF;



/* ***************************** End of functional FSM ***************************** */

 


END_FUNCTION_BLOCK


